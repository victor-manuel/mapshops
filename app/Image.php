<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Store;


class Image extends Model
{
    protected $fillable = [ 'pinture'];

		public function store(){
			return $this->hasOne(Store::class);
		}

}
