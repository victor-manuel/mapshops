<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Image;



class StoreController extends Controller

{

    protected function validator(array $data){
         return Validator::make($data, [
             'nombre' => ['required'],
             'direccion' => ['required'],
             'telefono' => ['required'],
             'mapa' => ['required'],
             'pinture_id' => ['required'],

         ]);
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   
        $store = \App\Store::all();
        return $store;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $imgShops = \App\Image::create([
            'pinture'=>''
        ]);
        if ($request->file('pinture')){
            $image = $request->file('pinture');
            $picShop = time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('Pics/'.$imgShops->id.'/'), $picShop);
            \App\Image::find($imgShops->id)->update([
                'pinture'=>$picShop
            ]);
        }
        
        $storeShops = \App\Store::create([
            'nombre'=> $request->input('nombre'),
            'direccion'=> $request->input('direccion'),
            'telefono'=> $request->input('telefono'),
            'email'=> $request->input('email'),
            'mapa'=> $request->input('mapa'),
            'pinture_id'=> $imgShops->id,
        ]);

            return $response;
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
