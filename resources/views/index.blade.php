<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>ShopsMap</title>

    
        <!-- Styles -->
                            <!-- estaticos -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
                            <!-- estaticos de boostrap -->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
                            <!-- estaticos  de font vue.js-->
        <link href="{{ asset('css/icons.css') }}" rel="stylesheet">


    </head>
    <body>
        <div id="main">  
            <conten-component></conten-component>
        </div>
       
                    <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" ></script>
        <script src="{{ asset('js/jquery.js') }}" ></script>
        <script src="{{ asset('js/popper.min.js') }}" ></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" ></script>

    </body>
</html>
