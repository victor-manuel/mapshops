import Vue from "vue";
import Router from "vue-router";
import Vuetify from "vuetify";

Vue.use(Router);
Vue.use(Vuetify);

export default new Router({
    routes: [
        {
            path: "/",
            name: "map",
            component: require("./views/admin/GoogleMapLoader.vue").default
        },
        {
            path: "/shops",
            name: "shops",
            component: require("./views/admin/store.vue").default
        }
    ],
    mode: "history"
});
